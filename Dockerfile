FROM PHP:7.1-alpine

LABEL maintainer="<Brodie Slater> github.com/brodster2"
LABEL purpose="debugging"

RUN apt-get update \
    && apt-get install apt-utils vim curl -y \
    && pecl install xdebug \
    && docker-php-ext-enable xdebug

COPY ./build/php.ini /usr/local/etc/php/

WORKDIR /var/www/html

EXPOSE 80

ENTRYPOINT [ "test-server" ]