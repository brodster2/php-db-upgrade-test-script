<?php

# To-do:
# 1. Parse numbers from files in specified directory and return highest number.

# 2. Get db version number from db

# 3. Compare numbers:
#   - if db version number == file number do nothing.
#   - if db number < file number run all scripts with higher number


# Set global variables
$directory = ""; # Needs to be an absolute path with a trailing "/"
$db_username = "";
$db_host = "";
$db_name = "";
$db_password = "";

# Set variables to values passed to script via commandline
# or display error if not enough arguments passed
if (count($argv) < 6) {
  echo "Error, required arguments are: \n
  Target directory
  Database username
  Database hostname
  Database name
  Database password
  ";
  die();
} else {
    $directory = $argv[1];
    $db_username = $argv[2];
    $db_host = $argv[3];
    $db_name = $argv[4];
    $db_password = $argv[5];
}

# Create database connection
try {
  $db_connect = new PDO("mysql:host=$db_host;dbname=$db_name", $db_username, $db_password);

  $db_connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e) {
  die("Couldn't connect to database: \n" . $e->getMessage());
}

/**
 * Use regex to extract a number from
 * specified filename and return it.
 *
 * @param string $filename
 *
 * @return int
 */
function getFileNumber($filename) {

  preg_match_all("/\d+/", $filename, $match);

  return (int)$match[0][0];
}

/**
 * Get contents of specified directory
 * sort the extract the file numbers with getFileNumbers()
 * and use the extracted number as the key in associative array
 * in order to sort the files.
 *
 * @param string $target_directory
 * 
 * @return array
 */
function getFileArray($target_directory) {
  # initialise empty array to add file number as key, and filename as value
  $scripts_array = array();

  # get array of all files in the target directory
  $dir_contents = scandir($target_directory);
  # loop through $dir_contents and extract numbers in filenames
  foreach ($dir_contents as $content) {

    $full_path = $target_directory . $content;

    # Check the content from directory is a file
    if (is_file($full_path)) {
      # hold the number found in file with regex
      $number = getFileNumber($content);
      
      # set number in file as array key for easy searching in future
      $scripts_array[$number] = $content;
    }
  }

  ksort($scripts_array);

  # return the populated array sorted by key
  return $scripts_array;
}

/**
 * Query the specified database and table for the database version number and return the number
 */
function getDbVersionNumber() {

  global $db_connect;

  # query database to get version number
  $stmt = $db_connect->query('SELECT version FROM versionTable');

  $result = $stmt->fetch(PDO::FETCH_ASSOC);

  $version = $result['version'];

  if (is_int($version)) {
    return $version;
  } else {
    return (int)$version;
  }
}

/**
 * Evaluate the database version number against
 * the highest number in the script files
 * and determine if any scripts need running
 * 
 * @param string $target_directory
 */
function checkVersion($target_directory) {
  # get highest key number in file array and cast to an integer
  $file_array = getFileArray($target_directory);

  end($file_array);
  $highest = (int) key($file_array);
  reset($file_array);

  # get the database version number
  $db_version = getDbVersionNumber();

  # check if db version is higher than highest script number
  if ($db_version >= $highest) {
    echo "Nothing to do, all up to date \n";
  } elseif($db_version < $highest) {
    runScripts($db_version, $highest);
    }
  }

  /**
   * If checkVersion() determines that scripts need to be run, use database version number
   * to determine which scripts in the array need to be run and then run them using PDO query()
   * 
   * @param int $version
   * @param int $highest
   * 
   */
  function runScripts($version, $highest) {

    global $directory, $db_connect;

    # get the sorted array of files in the specified directory
    $file_array = getFileArray($directory);

    # move array pointer to index of first file that needs running
    while(key($file_array) < $version) {
      next($file_array);
    }

    # Find the numeric position of the key in the associative array
    $i = array_search(key($file_array), array_keys($file_array));

    # get subset of scripts that need executing
    $to_execute = array_slice($file_array, $i);


    # execute any scripts with higher number than database version
    foreach ($to_execute as $script) {
      $query = file_get_contents($directory . $script);

      $stmt = $db_connect->prepare($query);

      if ($stmt->execute()) {
        echo "Executing script $script \n";
        continue;
      } else {
        throw new \Exception("Error Processing $script", 1);
        die();
      }
    }
    echo "All scripts executed. \n";
  }

  # run program
  checkVersion($directory);

  # Close database connection;
  $db_connect = NULL;
